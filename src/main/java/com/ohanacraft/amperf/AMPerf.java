package com.ohanacraft.amperf;

import com.ohanacraft.amperf.command.CommandTps;
import com.ohanacraft.amperf.command.CommandUptime;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid = AMPerf.MODID, name = AMPerf.NAME, version = AMPerf.VERSION, acceptableRemoteVersions = "*")
public class AMPerf
{
    public static final String MODID = "amperf";
    public static final String NAME = "AMPerf";
    public static final String VERSION = "1.12.2-v0.0.2";

    @EventHandler
    public void serverLoad(FMLServerStartingEvent event)
    {
        event.registerServerCommand(new CommandUptime());
        event.registerServerCommand(new CommandTps());
    }
}
