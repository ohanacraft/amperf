package com.ohanacraft.amperf.command;

import java.text.DecimalFormat;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class CommandTps extends CommandBase
{
    private static final DecimalFormat TIME_FORMATTER = new DecimalFormat("##.00");

    @Override
    public String getName()
    {
        return "tps";
    }

    @Override
    public String getUsage(ICommandSender sender)
    {
        return "command.tps.usage";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
    {
        double meanTickTime = mean(server.tickTimeArray) * 1.0E-6D;
        double meanTPS = Math.min(1000.0/meanTickTime, 20.0);
        sender.sendMessage(new TextComponentString("TPS from last 1m, 5m, 15m: " + TIME_FORMATTER.format(meanTPS) + ", 0.0, 0.0"));
    }

    private static long mean(long[] values)
    {
        long sum = 0L;
        for (long v : values)
        {
            sum += v;
        }
        return sum / values.length;
    }

    @Override
    public boolean checkPermission(MinecraftServer ms, ICommandSender ics)
    {
        return true;
    }
}