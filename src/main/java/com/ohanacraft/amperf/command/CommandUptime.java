package com.ohanacraft.amperf.command;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class CommandUptime extends CommandBase
{
    private final long start;

    public CommandUptime()
    {
        this.start = System.currentTimeMillis();
    }

    @Override
    public String getName() {
        return "uptime";
    }

    @Override
    public String getUsage(ICommandSender ics) {
        return "command.uptime.usage";
    }

    @Override
    public void execute(MinecraftServer ms, ICommandSender ics, String[] strings) throws CommandException {
        String uptime = "";
        long curtime = System.currentTimeMillis();
        long duration = (curtime - this.start) / 1000;

        ics.sendMessage(new TextComponentString("Server has been up for " + duration + " seconds"));
    }

    @Override
    public boolean checkPermission(MinecraftServer ms, ICommandSender ics)
    {
        return true;
    }
}
